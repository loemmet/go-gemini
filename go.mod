module github.com/makeworld-the-better-one/go-gemini

go 1.14

require (
	github.com/google/go-cmp v0.3.1
	github.com/vincent-petithory/dataurl v0.0.0-20191104211930-d1553a71de50 // indirect
	golang.org/x/net v0.0.0-20201216054612-986b41b23924
)

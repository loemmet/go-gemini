package gemini

import (
	"fmt"
	"net/url"
)

const (
	NimigemRequestMaxLength = 15360 //15kb limit to nimigem requests comprising "<url> <payload>"
)

// Additional Nimigem status code
const (
	StatusNimigemSuccess = 25
)

// All the statuses between 10 and 62 that are invalid
//same as gemini, except we disallow 1x and 20, and allow 25
var invalidNimigemStatuses = []int{
	10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
	20, 21, 22, 23, 24, 26, 27, 28, 29,
	32, 33, 34, 35, 36, 37, 38, 39,
	45, 46, 47, 48, 49,
	54, 55, 56, 57, 58,
}

// IsNimigemStatusValid checks whether an int status is covered by the Nimigem spec.
func IsNimigemStatusValid(status int) bool {
	if status < 10 || status > 62 {
		return false
	}
	for _, v := range invalidNimigemStatuses {
		if status == v {
			return false
		}
	}
	return true
}

//ValidateNimigemResponseMetaUrl does extra checks on the meta for specific status values. For example to check that
//returned URLs are of the required form (redirect only to nimigem, success 25 must be to gemini)
func ValidateNimigemResponseMetaUrl(meta string, status int) (success bool, err error) {
	var uri *url.URL

	uri, err = url.Parse(meta)
	if err != nil {
		err = fmt.Errorf("status did not return a valid URL: %s", meta)
		return
	}

	switch status {
	case StatusNimigemSuccess:
		if uri.Scheme != "gemini" {
			err = fmt.Errorf("nimigem status 25 did not include a gemini URL: %s", meta)
			return
		}
	case StatusRedirectPermanent, StatusRedirectTemporary:
		//must only redirect to nimigem
		if uri.Scheme != "nimigem" {
			err = fmt.Errorf("nimigem must only redirect to a nimigem URL: %s", meta)
			return
		}
	default:
		//shouldnt get here
		err = fmt.Errorf("unexpected nimigem status and meta passed for URL checking: %d %s", status, meta)
		return
	}

	//if we get this far all is OK
	return true, nil
}
